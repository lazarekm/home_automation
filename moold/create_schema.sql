
    create table devicestate (
       devicename varchar(50) not null,
        state varchar(255) not null,
        primary key (devicename)
    ) engine=InnoDB;

    create table deviceutilization (
       id bigint not null,
        devicename varchar(50) not null,
        offtimestamp datetime(6),
        ontimestamp datetime(6) not null,
        primary key (id)
    ) engine=InnoDB;

    create table hibernate_sequence (
       next_val bigint
    ) engine=InnoDB;

    insert into hibernate_sequence values ( 1 );

    insert into hibernate_sequence values ( 1 );

    create table temperature (
       id bigint not null,
        sensor_id varchar(255) not null,
        time_stamp datetime(6) not null,
        value decimal(19,2) not null,
        primary key (id)
    ) engine=InnoDB;

    alter table temperature 
       add constraint UK_fodo2j01a1mdh912t4ic7mgaq unique (sensor_id);
