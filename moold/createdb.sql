create database moold;
create user 'mooldapp'@'localhost' identified by 'mooldapp';
create user 'mooldro'@'localhost' identified by 'mooldro';
grant delete, insert, select, update on moold.* to 'mooldapp'@'localhost';
grant select on moold.* to 'mooldro'@'localhost';


