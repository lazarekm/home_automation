package miira.moold;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class DeviceNameTest {

    @Test
    void enumBasedDeviceNameExist() {
        Optional<DeviceName> heatingBathroom = DeviceName.getEnumByDeviceName("heating_bathroom");
        Assertions.assertTrue(heatingBathroom.isPresent());

        final DeviceName deviceName = heatingBathroom.get();
        assertThat("B.0", is(deviceName.getRelayId()));
    }

    @Test
    void enumBasedDeviceNameNotFound() {
        Optional<DeviceName> heatingBathroom = DeviceName.getEnumByDeviceName("unreal_device");
        Assertions.assertFalse(heatingBathroom.isPresent());
    }
}
