package miira.moold;

import miira.moold.model.DeviceState;
import miira.moold.model.DeviceUtilization;
import miira.moold.repository.DeviceStateRepository;
import miira.moold.repository.DeviceUtilizationRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.scheduling.TaskScheduler;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class DeviceControllerTest {

    private final String deviceName = "light_sink";

    private final DeviceUtilizationRepository deviceutilizationRepository;

    private final DeviceStateRepository deviceStateRepository;

    private final Mcp23017Api mcpApi;

    private final DeviceController deviceController;

    private final TaskScheduler taskScheduler;

    private final Clock clock = Clock.systemDefaultZone();

    public DeviceControllerTest() {
        deviceutilizationRepository = mock(DeviceUtilizationRepository.class);
        deviceStateRepository = mock(DeviceStateRepository.class);
        mcpApi = mock(Mcp23017Api.class);
        taskScheduler = mock(TaskScheduler.class);
        deviceController = new DeviceController(deviceStateRepository, deviceutilizationRepository, mcpApi, clock, taskScheduler);
    }

    private Optional<DeviceUtilization> createDeviceutilizationOptional() {
        DeviceUtilization deviceUtilization = new DeviceUtilization();
        deviceUtilization.setDeviceName(deviceName);
        deviceUtilization.setOnTimeStamp(LocalDateTime.now());
        return Optional.of(deviceUtilization);
    }

    private List<DeviceUtilization> createDeviceUtilizationList(
            String deviceName,
            LocalDateTime onTime,
            LocalDateTime offTime) {
        DeviceUtilization deviceUtilization = new DeviceUtilization();
        deviceUtilization.setDeviceName(deviceName);
        deviceUtilization.setOnTimeStamp(onTime);
        deviceUtilization.setOffTimeStamp(offTime);
        return Collections.singletonList(deviceUtilization);
    }

    @Test
    void getDeviceState_newDevice_returnNull() {
        final String deviceName = "newDevice";
        doReturn(null)
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        miira.moold.DeviceState deviceState = deviceController.getDeviceState(deviceName);

        assertNull(deviceState);
    }

    @Test
    void getDeviceState_existingDeviceWithOnState_returnDeviceOn() {
        final String deviceName = "light";
        final StateOnOff expectedState = StateOnOff.ON;
        doReturn(new DeviceState(deviceName, expectedState, LocalDateTime.now()))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        miira.moold.DeviceState deviceState = deviceController.getDeviceState(deviceName);

        assertThat(deviceState.getDeviceName(), is(deviceName));
        assertThat(deviceState.getState(), is(expectedState));
    }

    @Test
    void getDeviceState_existingDeviceWithOffState_returnDeviceOff() {
        final String deviceName = "light";
        final StateOnOff expectedState = StateOnOff.OFF;
        doReturn(new DeviceState(deviceName, expectedState, LocalDateTime.now()))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        miira.moold.DeviceState deviceState = deviceController.getDeviceState(deviceName);

        assertThat(deviceState.getDeviceName(), is(deviceName));
        assertThat(deviceState.getState(), is(expectedState));
    }

    @Test
    void switchReleased_switchIsOff_newUtilizationRecordCreated() {
        final String deviceName = "light_sink";
        LocalDateTime lastRelease = LocalDateTime.now().minus(10, ChronoUnit.MINUTES);
        doReturn(createDeviceUtilizationList(deviceName, LocalDateTime.now(), null))
                .when(deviceutilizationRepository)
                .findDeviceUtilizationByDeviceNameAndOffTimeStampIsNull(deviceName);
        doReturn(new DeviceState(deviceName, StateOnOff.OFF, lastRelease))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        deviceController.switchReleased(deviceName);

        ArgumentCaptor<DeviceUtilization> valueCapture = ArgumentCaptor.forClass(DeviceUtilization.class);
        verify(deviceutilizationRepository).saveAndFlush(valueCapture.capture());
        assertThat(valueCapture.getValue().getDeviceName(), is(deviceName));
        assertNotNull(valueCapture.getValue().getOnTimeStamp());
        assertNull(valueCapture.getValue().getOffTimeStamp());
    }

    @Test
    void switchReleased_switchIsOn_utilizationRecordFinished() {
        LocalDateTime lastRelease = LocalDateTime.now().minus(10, ChronoUnit.MINUTES);
        doReturn(createDeviceUtilizationList(deviceName, LocalDateTime.now(), null))
                .when(deviceutilizationRepository)
                .findDeviceUtilizationByDeviceNameAndOffTimeStampIsNull(deviceName);
        doReturn(new DeviceState(deviceName, StateOnOff.ON, lastRelease))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        deviceController.switchReleased(deviceName);

        ArgumentCaptor<DeviceUtilization> valueCapture = ArgumentCaptor.forClass(DeviceUtilization.class);
        verify(deviceutilizationRepository, times(1)).saveAndFlush(valueCapture.capture());
        DeviceUtilization finalUtilization = valueCapture.getAllValues().get(0);
        assertThat(finalUtilization.getDeviceName(), is(deviceName));
        assertNotNull(finalUtilization.getOnTimeStamp());
        assertNotNull(finalUtilization.getOffTimeStamp());
    }

    @Test
    void setDevice_deviceIsOffTryTurnOn_mcpCalled() {
        final String deviceName = "light";
        doReturn(new DeviceState(deviceName, StateOnOff.OFF, LocalDateTime.now()))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        deviceController.setDevice(deviceName, StateOnOff.ON);

        ArgumentCaptor<String> nameCapture = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<StateOnOff> stateCapture = ArgumentCaptor.forClass(StateOnOff.class);
        verify(mcpApi, times(1)).turnDevice(nameCapture.capture(), stateCapture.capture());
        assertThat(nameCapture.getValue(), is(deviceName));
        assertThat(stateCapture.getValue(), is(StateOnOff.ON));
    }

    @Test
    void setDevice_deviceIsOffTryTurnOn_stateOnAndNewUtilizationRecord() {
        final String deviceName = "light";
        doReturn(new DeviceState(deviceName, StateOnOff.OFF, LocalDateTime.now()))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        deviceController.setDevice(deviceName, StateOnOff.ON);

        ArgumentCaptor<DeviceState> stateCapture = ArgumentCaptor.forClass(DeviceState.class);
        verify(deviceStateRepository, times(1)).saveAndFlush(stateCapture.capture());
        assertThat(stateCapture.getValue().getDeviceName(), is(deviceName));
        assertThat(stateCapture.getValue().getState(), is(StateOnOff.ON));

        ArgumentCaptor<DeviceUtilization> utilizationCapture = ArgumentCaptor.forClass(DeviceUtilization.class);
        verify(deviceutilizationRepository).saveAndFlush(utilizationCapture.capture());
        assertThat(utilizationCapture.getValue().getDeviceName(), is(deviceName));
        assertNotNull(utilizationCapture.getValue().getOnTimeStamp());
        assertNull(utilizationCapture.getValue().getOffTimeStamp());
    }

    @Test
    void setDevice_deviceIsOnTryTurnOff_stateOffAndUtilizationRecordFinished() {
        final String deviceName = "light";
        doReturn(new DeviceState(deviceName, StateOnOff.ON, LocalDateTime.now()))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);
        doReturn(createDeviceUtilizationList(deviceName, LocalDateTime.now(), LocalDateTime.now()))
                .when(deviceutilizationRepository)
                .findDeviceUtilizationByDeviceNameAndOffTimeStampIsNull(deviceName);

        deviceController.setDevice(deviceName, StateOnOff.OFF);

        ArgumentCaptor<DeviceState> stateCapture = ArgumentCaptor.forClass(DeviceState.class);
        verify(deviceStateRepository, times(1)).saveAndFlush(stateCapture.capture());
        assertThat(stateCapture.getValue().getDeviceName(), is(deviceName));
        assertThat(stateCapture.getValue().getState(), is(StateOnOff.OFF));

        ArgumentCaptor<DeviceUtilization> utilizationCapture = ArgumentCaptor.forClass(DeviceUtilization.class);
        verify(deviceutilizationRepository).saveAndFlush(utilizationCapture.capture());
        assertThat(utilizationCapture.getValue().getDeviceName(), is(deviceName));
        assertNotNull(utilizationCapture.getValue().getOnTimeStamp());
        assertNotNull(utilizationCapture.getValue().getOffTimeStamp());
    }

    @Test
    void setDevice_deviceIsOffTryTurnOff_noPersistenceExecutedForStateAndUtilization() {
        final String deviceName = "light";
        doReturn(new DeviceState(deviceName, StateOnOff.OFF, LocalDateTime.now()))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        deviceController.setDevice(deviceName, StateOnOff.OFF);

        verify(mcpApi, never()).turnDevice(any(), any());
        verify(deviceStateRepository, never()).saveAndFlush(any());
        verify(deviceutilizationRepository, never()).findDeviceUtilizationByDeviceNameAndOffTimeStampIsNull(any());
        verify(deviceutilizationRepository, never()).saveAndFlush(any());
    }

    @Test
    void setDevice_deviceIsOnTryTurnOn_noPersistenceExecutedForStateAndUtilization() {
        final String deviceName = "light";
        doReturn(new DeviceState(deviceName, StateOnOff.ON, LocalDateTime.now()))
                .when(deviceStateRepository)
                .findDeviceStateByDeviceName(deviceName);

        deviceController.setDevice(deviceName, StateOnOff.ON);

        verify(mcpApi, never()).turnDevice(any(), any());
        verify(deviceStateRepository, never()).saveAndFlush(any());
        verify(deviceutilizationRepository, never()).findDeviceUtilizationByDeviceNameAndOffTimeStampIsNull(any());
        verify(deviceutilizationRepository, never()).saveAndFlush(any());
    }

    @Test
    void startBathroomFan_turnsOnClackImmediately() {
        final int durationInSeconds = 120;

        deviceController.startBathroomFan(durationInSeconds);

        ArgumentCaptor<String> deviceNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<StateOnOff> stateCaptor = ArgumentCaptor.forClass(StateOnOff.class);
        verify(mcpApi).turnDevice(deviceNameCaptor.capture(), stateCaptor.capture());
        assertThat(deviceNameCaptor.getValue(), is("fan_clack_bathroom"));
        assertThat(stateCaptor.getValue(), is(StateOnOff.ON));
    }

    @Test
    void startBathroomFan_schedulesFanToTurnOnAfter40Seconds() {
        final int durationInSeconds = 120;

        deviceController.startBathroomFan(durationInSeconds);

        ArgumentCaptor<Runnable> taskCaptor = ArgumentCaptor.forClass(Runnable.class);
        ArgumentCaptor<Instant> taskDateCaptor = ArgumentCaptor.forClass(Instant.class);
        verify(taskScheduler, times(2)).schedule(taskCaptor.capture(), taskDateCaptor.capture());

        double scheduledInMilli = taskDateCaptor.getAllValues().get(0).toEpochMilli();
        assertThat(scheduledInMilli, is(closeTo((double) (clock.millis() + 40000), 1000.0)));
    }

    @Test
    void startBathroomFan_schedulesFanAndClackToTurnOffAfterDuration() {
        final int durationInSeconds = 120;

        deviceController.startBathroomFan(durationInSeconds);

        ArgumentCaptor<Runnable> taskCaptor = ArgumentCaptor.forClass(Runnable.class);
        ArgumentCaptor<Instant> taskDateCaptor = ArgumentCaptor.forClass(Instant.class);
        verify(taskScheduler, times(2)).schedule(taskCaptor.capture(), taskDateCaptor.capture());

        double scheduledInMilli = taskDateCaptor.getAllValues().get(1).toEpochMilli();
        assertThat(scheduledInMilli, is(closeTo((double) (clock.millis() + 40000 + durationInSeconds * 1000L), 1000.0)));
    }
}
