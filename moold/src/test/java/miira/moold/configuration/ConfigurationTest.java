package miira.moold.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ConfigurationTest {

    @Test
    void basicDefinitions() {
        String confJson = "{\n" +
                "  \"switches\": [\n" +
                "    { \"name\":  \"sw_light_sink\", \"enabled\":  true },\n" +
                "    { \"name\":  \"sw_light_corridor\", \"enabled\":  false },\n" +
                "    { \"name\":  \"sw_light_bathroom\", \"enabled\":  false },\n" +
                "    { \"name\":  \"sw_light_bed\", \"enabled\":  false }\n" +
                "  ],\n" +
                "  \"devices\": [\n" +
                "    { \"name\": \"light_sink\", \"relayId\": \"B.4\" },\n" +
                "    { \"name\": \"light_corridor\", \"relayId\": \"B.6\" },\n" +
                "    { \"name\": \"light_bathroom\", \"relayId\": \"B.3\" },\n" +
                "    { \"name\": \"light_bed \", \"relayId\": \"B.5\" }\n" +
                "  ],\n" +
                "  \"clickSpecification\": {\n" +
                "    \"clickTypes\": [\n" +
                "      { \"name\": \"short\", \"durationMin\": 80, \"durationMax\": 800 },\n" +
                "      { \"name\": \"long\", \"durationMin\": 800, \"durationMax\": 2000 }\n" +
                "    ],\n" +
                "    \"doubleClickInterval\": 1200\n" +
                "  }\n" +
                "}";
        ObjectMapper mapper = new ObjectMapper();
        try {
            Configuration c = mapper.readValue(confJson, new TypeReference<Configuration>(){});

            assertEquals(4, c.getSwitches().size());

            String name = c.getSwitches().stream().filter(SwitchDef::isEnabled).findFirst().get().getName();
            assertEquals("sw_light_sink", name);

            assertEquals(4, c.getDevices().size());

            String relayId = c.getDevices().stream().filter(d -> d.getName().equals("light_bathroom")).findFirst().get().getRelayId();
            assertEquals("B.3", relayId);

            long doubleClickInterval = c.getClickSpecification().getDoubleClickInterval();
            assertEquals(1200, doubleClickInterval);

            long shortMaxMs = c.getClickSpecification().getClickTypes().stream().filter(t -> t.getName().equals("short")).findFirst().get().getDurationMaxMs();
            assertEquals(800, shortMaxMs);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private Configuration loadConfiguration(String jsonFileName) throws IOException {
        Path jsonPath = Paths.get("src/test/resources/" + jsonFileName);
        ObjectMapper mapper = new ObjectMapper();
        File jsonFile = jsonPath.toFile();
        return mapper.readValue(jsonFile, Configuration.class);
    }

    @Test
    void fullCorrectConfigurationValidation() throws IOException {
        Configuration configuration = loadConfiguration("correct_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        configuration.accept(validator);
    }

    @Test
    void duplicateSwitchValidation() throws IOException {
        Configuration configuration = loadConfiguration("duplicate_switch_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Duplicate switch name: sw_light_bathroom", exception.getLocalizedMessage());
    }

    @Test
    void duplicateDeviceValidation() throws IOException {
        Configuration configuration = loadConfiguration("duplicate_device_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Duplicate device name: light_bed", exception.getLocalizedMessage());
    }

    @Test
    void duplicateClickValidation() throws IOException {
        Configuration configuration = loadConfiguration("duplicate_click_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Duplicate switch name in clickType, name: short", exception.getLocalizedMessage());
    }

    @Test
    void emptySwitchValidation() throws IOException {
        Configuration configuration = loadConfiguration("empty_switch_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Switch name is empty", exception.getLocalizedMessage());
    }

    @Test
    void emptyDeviceValidation() throws IOException {
        Configuration configuration = loadConfiguration("empty_device_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Device name is empty", exception.getLocalizedMessage());
    }

    @Test
    void emptyClickSpecificationValidation() throws IOException {
        Configuration configuration = loadConfiguration("empty_click_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Click type name is empty", exception.getLocalizedMessage());
    }

    @Test
    void clickSpecificationOutOfRangeValidation() throws IOException {
        Configuration configuration = loadConfiguration("click_bad_values_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        IllegalStateException exception = assertThrows(
                IllegalStateException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Click \"long\" maximal duration is out of wise range <0, 3000>, actual value: 20000", exception.getLocalizedMessage());
    }

    @Test
    void undefSwitchInRulesValidation() throws IOException {
        Configuration configuration = loadConfiguration("undef_switch_in_rules_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        NoSuchElementException exception = assertThrows(
                NoSuchElementException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Switch rule refers unknown switch name: sw_light_sik", exception.getLocalizedMessage());
    }

    @Test
    void undefClickInRulesValidation() throws IOException {
        Configuration configuration = loadConfiguration("undef_click_in_rules_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        NoSuchElementException exception = assertThrows(
                NoSuchElementException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Switch rule refers unknown click name: shot", exception.getLocalizedMessage());
    }

    @Test
    void undefDeviceInRulesValidation() throws IOException {
        Configuration configuration = loadConfiguration("undef_device_in_rules_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        NoSuchElementException exception = assertThrows(
                NoSuchElementException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("ConditionAnd device refers unknown device name: light_sik", exception.getLocalizedMessage());
    }

    @Test
    void missingConditionInRulesValidation() throws IOException {
        Configuration configuration = loadConfiguration("missing_condition_in_rules_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        IllegalStateException exception = assertThrows(
                IllegalStateException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Missing condition for switch rule", exception.getLocalizedMessage());
    }

    @Test
    void missingRuleForSwitchValidation() throws IOException {
        Configuration configuration = loadConfiguration("missing_rule_for_switch_config.json");
        ConfigurationVisitor validator = new ConfigurationValidatorVisitor();
        IllegalStateException exception = assertThrows(
                IllegalStateException.class,
                () -> configuration.accept(validator)
        );
        assertEquals("Missing rule for switch: sw_light_corridor", exception.getLocalizedMessage());
    }
}
