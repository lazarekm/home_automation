package miira.moold.model;

import miira.moold.StateOnOff;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class StateOnOffConverterTest {

    private final StateOnOffConverter converter;

    public StateOnOffConverterTest() {
        this.converter = new StateOnOffConverter();
    }

    @Test
    void convertToDatabaseColumn_onState_stringOn() {
        String result = converter.convertToDatabaseColumn(StateOnOff.ON);

        assertThat(result, is("ON"));
    }

    @Test
    void convertToDatabaseColumn_offState_stringOff() {
        String result = converter.convertToDatabaseColumn(StateOnOff.OFF);

        assertThat(result, is("OFF"));
    }

    @Test
    void convertToEntityAttribute_stringOn_onState() {
        StateOnOff result = converter.convertToEntityAttribute("ON");

        assertThat(result, is(StateOnOff.ON));
    }

    @Test
    void convertToEntityAttribute_stringOff_offState() {
        StateOnOff result = converter.convertToEntityAttribute("OFF");

        assertThat(result, is(StateOnOff.OFF));
    }

    @Test
    void convertToEntityAttribute_wrongString_offState() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> converter.convertToEntityAttribute("wrong"));
    }
}
