package miira.moold.heating;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.nio.file.Path;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


class FlagFilesTest {

    private FlagFiles prepareMockedFlagFiles() {
        FlagFiles flagFiles = new FlagFiles() {
            @Override
            void checkFlagFiles() {
            }
        };
        FlagFiles mFlagFiles = Mockito.spy(flagFiles);
        mFlagFiles.prepareFlagStructure();
        return mFlagFiles;
    }

    @Test
    void getPath_bathroomOff_ReturnBathroomPath() {
        FlagFiles mFlagFiles = prepareMockedFlagFiles();
        RequestContext requestContext = new RequestContext(RequestContext.Device.BATHROOM_FLOOR, HeatingPhase.Type.OFF, 1);

        Path path = mFlagFiles.getPath(requestContext);

        assertThat(path.toString(), is(FlagFiles.OFF_FLAG_FILE_NAME));
    }

    @Test
    void getPath_bathroomOn_ReturnBathroomPath() {
        FlagFiles mFlagFiles = prepareMockedFlagFiles();
        RequestContext requestContext = new RequestContext(RequestContext.Device.BATHROOM_FLOOR, HeatingPhase.Type.ON, 1);

        Path path = mFlagFiles.getPath(requestContext);

        assertThat(path.toString(), is(FlagFiles.ON_FLAG_FILE_NAME));
    }

    @Test
    void getPath_bedroomOff_ReturnBedroomPath() {
        FlagFiles mFlagFiles = prepareMockedFlagFiles();
        RequestContext requestContext = new RequestContext(RequestContext.Device.RADIATOR_BEDROOM, HeatingPhase.Type.OFF, 1);

        Path path = mFlagFiles.getPath(requestContext);

        assertThat(path.toString(), is(FlagFiles.OFF_FLAG_RADIATOR_BEDROOM_FILE_NAME)
        );
    }

    @Test
    void getPath_bedroomOn_ReturnBedroomPath() {
        FlagFiles mFlagFiles = prepareMockedFlagFiles();
        RequestContext requestContext = new RequestContext(RequestContext.Device.RADIATOR_BEDROOM, HeatingPhase.Type.ON, 1);

        Path path = mFlagFiles.getPath(requestContext);

        assertThat(path.toString(), is(FlagFiles.ON_FLAG_RADIATOR_BEDROOM_FILE_NAME));
    }

    @Test
    void getPath_livingroomOff_ReturnLivingroomPath() {
        FlagFiles mFlagFiles = prepareMockedFlagFiles();
        RequestContext requestContext = new RequestContext(RequestContext.Device.RADIATOR_LIVINGROOM, HeatingPhase.Type.OFF, 1);

        Path path = mFlagFiles.getPath(requestContext);

        assertThat(path.toString(), is(FlagFiles.OFF_FLAG_RADIATOR_LIVINGROOM_FILE_NAME));

    }

    @Test
    void getPath_LivingroomOn_ReturnLivingroomPath() {
        FlagFiles mFlagFiles = prepareMockedFlagFiles();
        RequestContext requestContext = new RequestContext(RequestContext.Device.RADIATOR_LIVINGROOM, HeatingPhase.Type.ON, 1);

        Path path = mFlagFiles.getPath(requestContext);

        assertThat(path.toString(), is(FlagFiles.ON_FLAG_RADIATOR_LIVINGROOM_FILE_NAME));
    }
}
