package miira.moold.heating;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.*;

class ForceHeatingFileHelperTest {

    ForceHeatingFileHelper getSpyForceHeatingFileHelper() {
        final FlagFiles flagFiles = new FlagFiles() {
            @Override
            void checkFlagFiles() {
            }
        };
        final ForceHeatingFileHelper forceHeatingFileHelper = new ForceHeatingFileHelper(flagFiles);
        return spy(forceHeatingFileHelper);
    }

    @Test
    void getPathModifiedOffTest() {
        final Path path = Paths.get(FlagFiles.OFF_FLAG_FILE_NAME);
        ForceHeatingFileHelper fileHelper = Mockito.mock(ForceHeatingFileHelper.class);

        final LocalDateTime expectedTime = LocalDateTime
                .of(2020, 5, 19, 13, 50, 22, 555444);
        final Instant instant = expectedTime.atZone(ZoneId.systemDefault()).toInstant();
        final FileTime preparedFileTime = FileTime.from(instant);
        when(fileHelper.getPathModifiedFileTime(path)).thenReturn(preparedFileTime);
        when(fileHelper.getPathModified(any())).thenCallRealMethod();

        LocalDateTime resultTime = fileHelper.getPathModified(path);

        assertThat(resultTime, is(expectedTime));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            FlagFiles.ON_FLAG_FILE_NAME,
            FlagFiles.OFF_FLAG_FILE_NAME,
            FlagFiles.ON_FLAG_RADIATOR_BEDROOM_FILE_NAME,
            FlagFiles.OFF_FLAG_RADIATOR_BEDROOM_FILE_NAME,
            FlagFiles.ON_FLAG_RADIATOR_LIVINGROOM_FILE_NAME,
            FlagFiles.OFF_FLAG_RADIATOR_LIVINGROOM_FILE_NAME})
    void getPathModifiedOnTest(String flagFile) {
        final Path path = Paths.get(flagFile);
        ForceHeatingFileHelper fileHelper = Mockito.mock(ForceHeatingFileHelper.class);

        final LocalDateTime expectedTime = LocalDateTime
                .of(2020, 5, 19, 13, 50, 22, 555444);
        final Instant instant = expectedTime.atZone(ZoneId.systemDefault()).toInstant();
        final FileTime preparedFileTime = FileTime.from(instant);
        when(fileHelper.getPathModifiedFileTime(path)).thenReturn(preparedFileTime);
        when(fileHelper.getPathModified(any())).thenCallRealMethod();

        LocalDateTime resultTime = fileHelper.getPathModified(path);

        assertThat(resultTime, is(expectedTime));
    }

    @Test
    void postponePhase_postponeZeroHours_SameTimeReturned() {
        ForceHeatingFileHelper fileHelper = getSpyForceHeatingFileHelper();
        final LocalDateTime pathTime = LocalDateTime
                .of(2020, 5, 19, 13, 50, 22, 555444);
        final LocalDateTime expectedTime = LocalDateTime
                .of(2020, 5, 19, 13, 50, 22, 555444);
        doReturn(pathTime).when(fileHelper).getPathModified(any());
        doNothing().when(fileHelper).setPathModifiedFileTime(any(), any());
        RequestContext requestHeatingOnForOneHour = new RequestContext(RequestContext.Device.BATHROOM_FLOOR, HeatingPhase.Type.ON, 0);

        fileHelper.postponePhase(requestHeatingOnForOneHour);

        ArgumentCaptor<LocalDateTime> valueCapture = ArgumentCaptor.forClass(LocalDateTime.class);
        verify(fileHelper).setPathModified(any(), valueCapture.capture());
        assertThat(valueCapture.getValue(), is(expectedTime));
    }

    @Test
    void postponePhase_postponeOneHour_OneHourPotponed() {

        ForceHeatingFileHelper fileHelper = getSpyForceHeatingFileHelper();
        final LocalDateTime pathTime = LocalDateTime
                .of(2020, 5, 19, 13, 50, 22, 555444);
        final LocalDateTime expectedTime = LocalDateTime
                .of(2020, 5, 19, 14, 50, 22, 555444);
        doReturn(pathTime).when(fileHelper).getPathModified(any());
        doNothing().when(fileHelper).setPathModifiedFileTime(any(), any());
        RequestContext requestHeatingOnForOneHour = new RequestContext(RequestContext.Device.BATHROOM_FLOOR, HeatingPhase.Type.ON, 60);

        fileHelper.postponePhase(requestHeatingOnForOneHour);

        ArgumentCaptor<LocalDateTime> valueCapture = ArgumentCaptor.forClass(LocalDateTime.class);
        verify(fileHelper).setPathModified(any(), valueCapture.capture());
        assertThat(valueCapture.getValue(), is(expectedTime));
    }

    @Test
    void postponePhase_postpone41Hours_41HourPotponed() {

        ForceHeatingFileHelper fileHelper = getSpyForceHeatingFileHelper();
        final LocalDateTime pathTime = LocalDateTime
                .of(2020, 5, 19, 13, 50, 22, 555444);
        final LocalDateTime expectedTime = LocalDateTime
                .of(2020, 5, 21, 6, 50, 22, 555444);
        doReturn(pathTime).when(fileHelper).getPathModified(any());
        doNothing().when(fileHelper).setPathModifiedFileTime(any(), any());
        RequestContext requestHeatingOnForOneHour = new RequestContext(RequestContext.Device.BATHROOM_FLOOR, HeatingPhase.Type.ON, 41*60);

        fileHelper.postponePhase(requestHeatingOnForOneHour);

        ArgumentCaptor<LocalDateTime> valueCapture = ArgumentCaptor.forClass(LocalDateTime.class);
        verify(fileHelper).setPathModified(any(), valueCapture.capture());
        assertThat(valueCapture.getValue(), is(expectedTime));
    }
}
