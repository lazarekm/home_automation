package miira.moold.heating;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static miira.moold.heating.HeatingPhase.Type.OFF;
import static miira.moold.heating.HeatingPhase.Type.ON;
import static org.mockito.Mockito.*;

class HeatingForcePhaseManagerTest {

    @ParameterizedTest
    @MethodSource("provideSamePhaseParameters")
    void prolongSamePhase(HeatingPhase.Type inputParam, List<HeatingPhase> phasesSetup, VerificationMode resetPhaseVerification) {
        final RequestContext.Device device = RequestContext.Device.BATHROOM_FLOOR;
        final RequestContext context = new RequestContext(device, inputParam, 1);
        ForceHeatingHelper heatingHelper = Mockito.mock(ForceHeatingHelper.class);
        HeatingForcePhaseManager manager = new HeatingForcePhaseManager(heatingHelper);

        when(heatingHelper.getPhases(device)).thenReturn(phasesSetup);

        manager.prolongPhase(context);

        verify(heatingHelper, resetPhaseVerification).resetPhase(context);
        verify(heatingHelper, times(1)).postponePhase(context);
    }

    private static Stream<Arguments> provideSamePhaseParameters() {
        LocalDateTime past = LocalDateTime.now().minus(1, ChronoUnit.YEARS);
        LocalDateTime future = LocalDateTime.now().plus(1, ChronoUnit.YEARS);
        return Stream.of(
                Arguments.of(
                        ON,
                        Arrays.asList(new HeatingPhase(OFF, past), new HeatingPhase(ON, past)),
                        times(1)), // activate
                Arguments.of(
                        OFF,
                        Arrays.asList(new HeatingPhase(OFF, past), new HeatingPhase(ON, past)),
                        times(1)), // activate
                Arguments.of(
                        ON,
                        Arrays.asList(new HeatingPhase(OFF, past), new HeatingPhase(ON, future)),
                        never()), // postpone active
                Arguments.of(OFF,
                        Arrays.asList(new HeatingPhase(OFF, future), new HeatingPhase(ON, past)),
                        never()) // postpone active
        );
    }

    @ParameterizedTest
    @MethodSource("provideOppositePhaseParameters")
    void prolongOppositePhase(HeatingPhase.Type inputParam, List<HeatingPhase> phasesSetup) {
        final RequestContext.Device device = RequestContext.Device.BATHROOM_FLOOR;
        final RequestContext context = new RequestContext(device, inputParam, 1);
        ForceHeatingHelper heatingHelper = Mockito.mock(ForceHeatingHelper.class);
        HeatingForcePhaseManager manager = new HeatingForcePhaseManager(heatingHelper);

        when(heatingHelper.getPhases(device)).thenReturn(phasesSetup);

        manager.prolongPhase(context);

        verify(heatingHelper, times(2)).resetPhase(any());
        verify(heatingHelper, never()).postponePhase(any());
    }

    private static Stream<Arguments> provideOppositePhaseParameters() {
        LocalDateTime past = LocalDateTime.now().minus(1, ChronoUnit.YEARS);
        LocalDateTime future = LocalDateTime.now().plus(1, ChronoUnit.YEARS);
        return Stream.of(
                Arguments.of(ON, Arrays.asList(new HeatingPhase(OFF, future), new HeatingPhase(ON, past))), // postpone inactive
                Arguments.of(OFF, Arrays.asList(new HeatingPhase(OFF, past), new HeatingPhase(ON, future))) // postpone inactive
        );
    }
}