package miira.moold;

import miira.moold.heating.HeatingForcePhaseManager;
import miira.moold.heating.HeatingPhase;
import miira.moold.heating.RequestContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;

import java.util.Locale;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class RestApiTest {

    private final RestApi restApi;

    private final HeatingForcePhaseManager heatingManager;

    private final DeviceController deviceController;

    public RestApiTest() {
        deviceController = mock(DeviceController.class);
        heatingManager = mock(HeatingForcePhaseManager.class);
        restApi = new RestApi(deviceController, heatingManager);
    }

    @ParameterizedTest
    @ValueSource(strings = {"on", "off", "ON", "OFF", " On", "off "})
    void setHeatingTimeTest(final String state) {
        final RequestContext.Device device = RequestContext.Device.BATHROOM_FLOOR;

        restApi.setHeatingTime(state, Optional.of(2));

        ArgumentCaptor<RequestContext> valueCapture = ArgumentCaptor.forClass(RequestContext.class);
        verify(heatingManager).prolongPhase(valueCapture.capture());

        assertThat(valueCapture.getValue().getDevice(), is(device));
        assertThat(valueCapture.getValue().getAdditionalMinutes(), is(120L));
        if (state.toLowerCase(Locale.ROOT).trim().equals("on")) {
            assertThat(valueCapture.getValue().getPhase(), is(HeatingPhase.Type.ON));
        } else {
            assertThat(valueCapture.getValue().getPhase(), is(HeatingPhase.Type.OFF));
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"on", "off", "ON", "OFF", " On", "off "})
    void setHeatingTime_nullHours_expectedOne(final String state) {
        final RequestContext.Device device = RequestContext.Device.BATHROOM_FLOOR;

        restApi.setHeatingTime(state, Optional.empty());

        ArgumentCaptor<RequestContext> valueCapture = ArgumentCaptor.forClass(RequestContext.class);
        verify(heatingManager).prolongPhase(valueCapture.capture());

        assertThat(valueCapture.getValue().getDevice(), is(device));
        assertThat(valueCapture.getValue().getAdditionalMinutes(), is(60L));
        if (state.toLowerCase(Locale.ROOT).trim().equals("on")) {
            assertThat(valueCapture.getValue().getPhase(), is(HeatingPhase.Type.ON));
        } else {
            assertThat(valueCapture.getValue().getPhase(), is(HeatingPhase.Type.OFF));
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "   ", "xxx", "ON OFF"})
    void setHeatingTimeTestFail(final String state) {
        final RequestContext.Device device = RequestContext.Device.BATHROOM_FLOOR;

        restApi.setHeatingTime(state, Optional.of(1));

        ArgumentCaptor<RequestContext> valueCapture = ArgumentCaptor.forClass(RequestContext.class);
        verify(heatingManager).prolongPhase(valueCapture.capture());

        assertThat(valueCapture.getValue().getDevice(), is(device));
        assertThat(valueCapture.getValue().getPhase(), is(HeatingPhase.Type.OFF));
    }

    @ParameterizedTest
    @ValueSource(strings = { "ON", "On", "on"})
    void setDevice_parameterStateON_setDevicePassOnEnum(String stateOn) {
        final String deviceName = "light";
        restApi.setDevice(deviceName, stateOn);

        ArgumentCaptor<StateOnOff> captureState = ArgumentCaptor.forClass(StateOnOff.class);
        verify(deviceController).setDevice(eq(deviceName), captureState.capture());
        assertThat(captureState.getValue(), is(StateOnOff.ON));
    }

    @ParameterizedTest
    @ValueSource(strings = { "OFF", "Off", "off"})
    void setDevice_parameterStateOFF_setDevicePassOffEnum(String stateOff) {
        final String deviceName = "light";
        restApi.setDevice(deviceName, stateOff);

        ArgumentCaptor<StateOnOff> captureState = ArgumentCaptor.forClass(StateOnOff.class);
        verify(deviceController).setDevice(eq(deviceName), captureState.capture());
        assertThat(captureState.getValue(), is(StateOnOff.OFF));
    }

    @ParameterizedTest
    @ValueSource(strings = { "", "wrong"})
    void setDevice_parameterStateWrong_setDevicePassOffEnum(String stateOff) {
        final String deviceName = "light";
        restApi.setDevice(deviceName, stateOff);

        ArgumentCaptor<StateOnOff> captureState = ArgumentCaptor.forClass(StateOnOff.class);
        verify(deviceController).setDevice(eq(deviceName), captureState.capture());
        assertThat(captureState.getValue(), is(StateOnOff.OFF));
    }
}
