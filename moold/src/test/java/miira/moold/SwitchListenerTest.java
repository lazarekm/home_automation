package miira.moold;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.impl.GpioPinImpl;
import com.pi4j.io.gpio.impl.PinImpl;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.EnumSet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class SwitchListenerTest {
    private final DeviceController relayController = mock(DeviceController.class);
    GpioPinDigitalInput pinSink = mock(GpioPinDigitalInput.class);
    GpioPinDigitalInput pinCorridor23 = mock(GpioPinDigitalInput.class);
    GpioPinDigitalInput pinCorridor08 = mock(GpioPinDigitalInput.class);
    GpioPinDigitalInput pinBedroom = mock(GpioPinDigitalInput.class);
    GpioPinDigitalInput pinBedroomLeft = mock(GpioPinDigitalInput.class);
    private SwitchListener switchListener;

    private void setupListener() {
        final RaspberryFactory gpioFactory = mock(RaspberryFactory.class);
        final GpioController gpioController = mock(GpioController.class);
        final GpioProvider gpioProvider = mock(GpioProvider.class);
        when(gpioFactory.getInstance()).thenReturn(gpioController);
        when(gpioFactory.getGpioProvider()).thenReturn(gpioProvider);
        when(gpioController.provisionDigitalInputPin(gpioProvider, RaspiPin.GPIO_18, "light_sink", PinPullResistance.OFF)).thenReturn(pinSink);
        when(gpioController.provisionDigitalInputPin(gpioProvider, RaspiPin.GPIO_23, "light_corridor", PinPullResistance.OFF)).thenReturn(pinCorridor23);
        //when(gpioController.provisionDigitalInputPin(gpioProvider, RaspiPin.GPIO_08, "light_corridor", PinPullResistance.OFF)).thenReturn(pinCorridor08);
        when(gpioController.provisionDigitalInputPin(gpioProvider, RaspiPin.GPIO_12, "light_bed", PinPullResistance.OFF)).thenReturn(pinBedroom);
        when(gpioController.provisionDigitalInputPin(gpioProvider, RaspiPin.GPIO_16, "light_bedroom_left", PinPullResistance.OFF)).thenReturn(pinBedroomLeft);
        switchListener = new SwitchListener(gpioFactory, relayController);
    }

    private GpioPinDigitalStateChangeEvent createEvent(final int address, final String name, PinState state) {
        PinImpl digitalPin = new PinImpl(null, address, name, EnumSet.of(PinMode.DIGITAL_INPUT));
        GpioPinImpl gpioPin = new GpioPinImpl(null, null, digitalPin);
        return new GpioPinDigitalStateChangeEvent(
                new Object(),
                gpioPin,
                state);
    }

    @Test
    void postConstruct() {
        setupListener();

        switchListener.postConstruct();

        verify(pinSink, times(1)).setDebounce(anyInt());
        verify(pinSink, times(1)).addListener((GpioPinListener) any());
//        verify(pinCorridor08, times(1)).setDebounce(anyInt());
//        verify(pinCorridor08, times(1)).addListener((GpioPinListener) any());
        verify(pinCorridor23, times(1)).setDebounce(anyInt());
        verify(pinCorridor23, times(1)).addListener((GpioPinListener) any());
        verify(pinBedroom, times(1)).setDebounce(anyInt());
        verify(pinBedroom, times(1)).addListener((GpioPinListener) any());
        verify(pinBedroomLeft, times(1)).setDebounce(anyInt());
        verify(pinBedroomLeft, times(1)).addListener((GpioPinListener) any());
    }

    @Test
    void eventLow_switchReleasedCalled() {
        setupListener();
        ArgumentCaptor<GpioPinListenerDigital> controllerCaptor = ArgumentCaptor.forClass(GpioPinListenerDigital.class);
        switchListener.postConstruct();
        verify(pinSink).addListener(controllerCaptor.capture());
        GpioPinDigitalStateChangeEvent event = createEvent(18, "gpio 18", PinState.LOW);

        controllerCaptor.getValue().handleGpioPinDigitalStateChangeEvent(event);

        verify(relayController, times(1)).switchReleased("light_sink");
    }

    @Test
    void eventHigh_switchReleasedDoNotCalled() {
        setupListener();
        ArgumentCaptor<GpioPinListenerDigital> controllerCaptor = ArgumentCaptor.forClass(GpioPinListenerDigital.class);
        switchListener.postConstruct();
        verify(pinSink).addListener(controllerCaptor.capture());
        GpioPinDigitalStateChangeEvent event = createEvent(18, "gpio 18", PinState.HIGH);

        controllerCaptor.getValue().handleGpioPinDigitalStateChangeEvent(event);

        verify(relayController, times(0)).switchReleased(anyString());
    }

    @Test
    void eventLow_switchCorridor23ReleasedCalled() {
        setupListener();
        ArgumentCaptor<GpioPinListenerDigital> controllerCaptor = ArgumentCaptor.forClass(GpioPinListenerDigital.class);
        switchListener.postConstruct();
        verify(pinSink).addListener(controllerCaptor.capture());
        GpioPinDigitalStateChangeEvent event = createEvent(23, "gpio 23", PinState.LOW);

        controllerCaptor.getValue().handleGpioPinDigitalStateChangeEvent(event);

        verify(relayController, times(1)).switchReleased("light_corridor");
    }

    @Test
    void eventLow_switchCorridor08ReleasedCalled() {
        setupListener();
        ArgumentCaptor<GpioPinListenerDigital> controllerCaptor = ArgumentCaptor.forClass(GpioPinListenerDigital.class);
        switchListener.postConstruct();
        verify(pinSink).addListener(controllerCaptor.capture());
        GpioPinDigitalStateChangeEvent event = createEvent(8, "gpio 08", PinState.LOW);

        controllerCaptor.getValue().handleGpioPinDigitalStateChangeEvent(event);

        verify(relayController, times(1)).switchReleased("light_corridor");
    }

    @Test
    void eventLow_switchBedroomReleasedCalled() {
        setupListener();
        ArgumentCaptor<GpioPinListenerDigital> controllerCaptor = ArgumentCaptor.forClass(GpioPinListenerDigital.class);
        switchListener.postConstruct();
        verify(pinSink).addListener(controllerCaptor.capture());
        GpioPinDigitalStateChangeEvent event = createEvent(12, "gpio 12", PinState.LOW);

        controllerCaptor.getValue().handleGpioPinDigitalStateChangeEvent(event);

        verify(relayController, times(1)).switchReleased("light_bed");
    }

    @Test
    void eventLow_switchBedroomLeftReleasedCalled() {
        setupListener();
        ArgumentCaptor<GpioPinListenerDigital> controllerCaptor = ArgumentCaptor.forClass(GpioPinListenerDigital.class);
        switchListener.postConstruct();
        verify(pinSink).addListener(controllerCaptor.capture());
        GpioPinDigitalStateChangeEvent event = createEvent(16, "gpio 16", PinState.LOW);

        controllerCaptor.getValue().handleGpioPinDigitalStateChangeEvent(event);

        verify(relayController, times(1)).switchReleased("light_bedroom_left");
    }
}