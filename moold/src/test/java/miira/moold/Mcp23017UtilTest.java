package miira.moold;

import com.pi4j.io.gpio.Pin;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Mcp23017UtilTest {

    private final Mcp23017Util util;

    public Mcp23017UtilTest() {
        this.util = new Mcp23017Util();
    }

    @Test
    void getPinByName_nullGpioName_returnEmptyOption() {
        Optional<Pin> resultPin = util.getPinByName(null);

        assertFalse(resultPin.isPresent());
    }

    @Test
    void getPinByName_emptyGpioName_returnEmptyOption() {
        Optional<Pin> resultPin = util.getPinByName("");

        assertFalse(resultPin.isPresent());
    }

    @Test
    void getPinByName_nonExistingGpioName_returnEmptyOption() {
        Optional<Pin> resultPin = util.getPinByName("wrong name");

        assertFalse(resultPin.isPresent());
    }

    @Test
    void getPinByName_shortLowerCaseGpioName_returnPin() {
        Optional<Pin> resultPin = util.getPinByName("a1");

        assertTrue(resultPin.isPresent());
        assertThat(resultPin.get().getName(), is("GPIO A1"));
    }

    @Test
    void getPinByName_shortUpperCaseGpioName_returnPin() {
        Optional<Pin> resultPin = util.getPinByName("A1");

        assertTrue(resultPin.isPresent());
        assertThat(resultPin.get().getName(), is("GPIO A1"));
    }

    @Test
    void getPinByName_fullNameLowerCaseGpioName_returnPin() {
        Optional<Pin> resultPin = util.getPinByName("gpio a1");

        assertTrue(resultPin.isPresent());
        assertThat(resultPin.get().getName(), is("GPIO A1"));
    }

    @Test
    void getPinByName_fullNameUpperCaseGpioName_returnPin() {
        Optional<Pin> resultPin = util.getPinByName("GPIO A1");

        assertTrue(resultPin.isPresent());
        assertThat(resultPin.get().getName(), is("GPIO A1"));
    }

    @Test
    void getPinByName_fullNameHighBankGpioName_returnPin() {
        Optional<Pin> resultPin = util.getPinByName("GPIO B1");

        assertTrue(resultPin.isPresent());
        assertThat(resultPin.get().getName(), is("GPIO B1"));
    }
}
