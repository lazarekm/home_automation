package miira.moold;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.pi4j.io.i2c.I2CFactory;
import miira.moold.configuration.Mcp23017;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class Mcp23017ApiTest {

    private final Mcp23017 configuration = mock(Mcp23017.class);
    private final RaspberryFactory factory = mock(RaspberryFactory.class);
    private final Mcp23017Util mcpUtil = mock(Mcp23017Util.class);

    private ListAppender<ILoggingEvent> appender;
    private final Logger appLogger = (Logger) LoggerFactory.getLogger(Mcp23017Api.class);

    @BeforeEach
    public void setUp() {
        appender = new ListAppender<>();
        appender.start();
        appLogger.addAppender(appender);
    }

    @AfterEach
    public void tearDown() {
        appLogger.detachAppender(appender);
    }

    @Test
    void construct_wrongGpioName_errorLogged() throws IOException, I2CFactory.UnsupportedBusNumberException {
        final String deviceName = "sink_light";
        final String gpioName = "wrong gpio name";
        Mockito.when(configuration.getGpioByDeviceName()).thenReturn(
                new HashMap<String, String>() {{
                    put(deviceName, gpioName);
                }}
        );
        Mockito.when(mcpUtil.getPinByName("wrong gpio name")).thenReturn(Optional.empty());
        Mockito.when(factory.getInstance()).thenReturn(null);

        new Mcp23017Api(configuration, factory, mcpUtil);

        assertThat(appender.list)
                .extracting(ILoggingEvent::getFormattedMessage)
                .contains("Gpio not configured, name not found: " + gpioName);
    }
}
