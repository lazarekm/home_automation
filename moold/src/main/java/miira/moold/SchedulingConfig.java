package miira.moold;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

// We need this class for enable scheduling for the application.
@Configuration
@EnableScheduling
public class SchedulingConfig {
}
