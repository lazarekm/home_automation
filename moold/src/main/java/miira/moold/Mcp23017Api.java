package miira.moold;

import com.pi4j.gpio.extension.mcp.MCP23017GpioProvider;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import miira.moold.configuration.Mcp23017;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class Mcp23017Api {
    private static final Logger logger = LoggerFactory.getLogger(Mcp23017Api.class);

    private final Mcp23017 configuration;
    private final RaspberryFactory factory;
    private final Mcp23017Util mcpUtil;
    private GpioController gpioController;
    private MCP23017GpioProvider provider;
    private final Map<String, GpioPinDigitalOutput> gpioByDeviceName = new HashMap<>();

    @Autowired
    public Mcp23017Api(Mcp23017 configuration, RaspberryFactory factory, Mcp23017Util mcpUtil) throws IOException, UnsupportedBusNumberException {
        this.configuration = configuration;
        this.factory = factory;
        this.mcpUtil = mcpUtil;
        initialize();
    }

    void initialize() throws UnsupportedBusNumberException, IOException {
        gpioController = factory.getInstance();
        provider = factory.getMcp23017Provider(configuration.getBusNumber(), configuration.getAddress());

        for (Map.Entry<String, String> entry : configuration.getGpioByDeviceName().entrySet()) {
            final String gpioName = entry.getValue();
            Optional<Pin> pin = mcpUtil.getPinByName(gpioName);
            if (pin.isPresent()) {
                final String deviceName = entry.getKey();
                configureGpio(deviceName, pin.get());
            } else {
                logger.error("Gpio not configured, name not found: {}", gpioName);
            }
        }
    }

    @PreDestroy
    void postConstruct() {
        logger.info("Shutting down Gpio controller");
        gpioController.shutdown();
    }

    private void configureGpio(final String deviceName, final Pin gpio) {
        logger.info("Configure device {} to gpio {}", deviceName, gpio.getName());
        GpioPinDigitalOutput digitalOutput = gpioController.provisionDigitalOutputPin(provider, gpio, deviceName, PinState.LOW);
        gpioByDeviceName.put(deviceName, digitalOutput);
    }

    public void turnDevice(String deviceName, StateOnOff requestedState) {
        if (gpioByDeviceName.containsKey(deviceName)) {
            if (requestedState == StateOnOff.ON) {
                logger.info("Set device {} to high", deviceName);
                gpioByDeviceName.get(deviceName).high();
            } else if (requestedState == StateOnOff.OFF) {
                logger.info("Set device {} to low", deviceName);
                gpioByDeviceName.get(deviceName).low();
            } else {
                logger.error("Requested requestedState not supported: {}", requestedState);
            }
        } else {
            logger.error("Device not configured: {}", deviceName);
        }
    }
}
