package miira.moold;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DeviceState {
    private final String deviceName;
    private final StateOnOff state;
}