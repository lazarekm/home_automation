package miira.moold;

import miira.moold.model.DeviceUtilization;
import miira.moold.repository.DeviceStateRepository;
import miira.moold.repository.DeviceUtilizationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * This component route requests to devices directly using the MCP chip.
 * It persists state of devices and provide state to upper level layers.
 */
@Component
public class DeviceController {

    private static final LocalDateTime minimalMariaDBTime = LocalDateTime.of(1000, Month.JANUARY, 1, 0, 0);

    private final Logger logger = LoggerFactory.getLogger(DeviceController.class);

    private final DeviceStateRepository deviceStateRepository;

    private final DeviceUtilizationRepository deviceutilizationRepository;

    private final Mcp23017Api mcpApi;

    private final Clock clock;

    private final TaskScheduler taskScheduler;

    @Autowired
    public DeviceController(DeviceStateRepository deviceStateRepository, DeviceUtilizationRepository deviceutilizationRepository, Mcp23017Api mcpApi, Clock clock, TaskScheduler taskScheduler) {
        this.deviceStateRepository = deviceStateRepository;
        this.deviceutilizationRepository = deviceutilizationRepository;
        this.mcpApi = mcpApi;
        this.clock = clock;
        this.taskScheduler = taskScheduler;
    }

    public DeviceState getDeviceState(String deviceName) {
        miira.moold.model.DeviceState deviceState = deviceStateRepository.findDeviceStateByDeviceName(deviceName);
        if (deviceState == null) {
            logger.error("Device state not found for {}", deviceName);
            return null;
        }
        return new DeviceState(deviceName, deviceState.getState());
    }

    synchronized public void switchReleased(String deviceName) {
        /*
        light_sink = B.4
        light_bathroom = B.3
        light_bed = B.5
        light_corridor = B.6
        light_bedroom_right = B.2
        light_bedroom_left = B.1
        heating_bathroom = B.0
        fan_clack_bathroom = A.2
        fan_bathroom = A.1
         */
        miira.moold.model.DeviceState deviceState = deviceStateRepository.findDeviceStateByDeviceName(deviceName);
        LocalDateTime nextEarliestRelease = deviceState.getLastUpdate().plus(1500, ChronoUnit.MILLIS);
        if (LocalDateTime.now(clock).isAfter(nextEarliestRelease)) {
            StateOnOff requestedState = deviceState == null || deviceState.getState() == StateOnOff.ON ? StateOnOff.OFF : StateOnOff.ON;
            turnDevice(deviceName, requestedState);
        }
    }

    private Optional<DeviceUtilization> getDeviceUtilization(String deviceName, StateOnOff requestedState) {
        DeviceUtilization deviceUtilization = null;
        if (requestedState == StateOnOff.ON) {
            deviceUtilization = new DeviceUtilization();
            deviceUtilization.setDeviceName(deviceName);
            deviceUtilization.setOnTimeStamp(LocalDateTime.now());
        } else {
            List<DeviceUtilization> utilizations = deviceutilizationRepository.findDeviceUtilizationByDeviceNameAndOffTimeStampIsNull(deviceName);
            if (utilizations.isEmpty()) {
                logger.error("No open utilization found for device {}", deviceName);
            } else if (utilizations.size() > 1) {
                logger.error("Found more than one open utilization for device {}", deviceName);
                utilizations.forEach(u -> {
                    u.setOffTimeStamp(minimalMariaDBTime);
                    deviceutilizationRepository.saveAndFlush(u);
                });
            } else {
                deviceUtilization = utilizations.get(0);
                deviceUtilization.setOffTimeStamp(LocalDateTime.now());
            }
        }
        return Optional.ofNullable(deviceUtilization);
    }

    public void setDevice(String deviceName, StateOnOff requestedState) {
        if (deviceName.equals("all")) {
            String[] devices = {"light_corridor", "light_sink", "light_bathroom", "light_bed", "light_bedroom_right", "light_bedroom_left", "heating_bathroom"};
            Arrays.stream(devices).forEach(x -> turnDevice(deviceName, StateOnOff.OFF));
        } else {
            // TODO: validate device name and validate requested state
            miira.moold.model.DeviceState deviceState = deviceStateRepository.findDeviceStateByDeviceName(deviceName);
            if (deviceState.getState() == requestedState) {
                logger.warn("Device state is {}, requested state is {}, let's do nothing.", deviceState.getState(), requestedState);
            } else {
                turnDevice(deviceName, requestedState);
            }
        }
    }

    private void turnDevice(String deviceName, StateOnOff requestedState) {
        mcpApi.turnDevice(deviceName, requestedState);
        miira.moold.model.DeviceState deviceState = new miira.moold.model.DeviceState(deviceName, requestedState, LocalDateTime.now(clock));
        deviceStateRepository.saveAndFlush(deviceState);
        Optional<DeviceUtilization> deviceUtilization = getDeviceUtilization(deviceName, requestedState);
        deviceUtilization.ifPresent(utilization -> deviceutilizationRepository.saveAndFlush(utilization));
    }

    public void startBathroomFan(int durationInSeconds) {
        turnDevice("fan_clack_bathroom", StateOnOff.ON);

        int delayStartInSeconds = 40;
        Instant now = clock.instant();
        taskScheduler.schedule(() -> turnDevice("fan_bathroom", StateOnOff.ON), now.plusSeconds(delayStartInSeconds));

        taskScheduler.schedule(() -> {
            turnDevice("fan_bathroom", StateOnOff.OFF);
            turnDevice("fan_clack_bathroom", StateOnOff.OFF);
        }, now.plusSeconds(durationInSeconds + delayStartInSeconds));
    }
}
