package miira.moold;

import com.pi4j.gpio.extension.mcp.MCP23017GpioProvider;
import com.pi4j.io.gpio.*;
import com.pi4j.io.i2c.I2CFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RaspberryFactory {

    public synchronized GpioController getInstance() {
        return GpioFactory.getInstance();
    }

    public GpioProvider getGpioProvider() {
        return new RaspiGpioProvider(RaspiPinNumberingScheme.BROADCOM_PIN_NUMBERING);
    }

    public MCP23017GpioProvider getMcp23017Provider(final int busNumber, final int i2cAddress) throws IOException, I2CFactory.UnsupportedBusNumberException {
        return new MCP23017GpioProvider(busNumber, i2cAddress);
    }
}
