package miira.moold;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MooldMain {

    public static void main(String[] args) {
        SpringApplication.run(MooldMain.class, args);
    }
}
