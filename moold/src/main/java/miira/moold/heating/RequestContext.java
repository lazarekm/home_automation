package miira.moold.heating;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RequestContext {

    public enum Device {
        BATHROOM_FLOOR,
        RADIATOR_BEDROOM,
        RADIATOR_LIVINGROOM
    }

    private final Device device;

    private final HeatingPhase.Type phase;

    private final long additionalMinutes;
}
