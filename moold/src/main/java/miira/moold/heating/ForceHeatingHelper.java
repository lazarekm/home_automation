package miira.moold.heating;

import java.util.List;

/**
 * Interface provide functionality for setup end time of hot or cool phase.
 */
public interface ForceHeatingHelper {
    /**
     * @return current phases setup.
     * @param device
     */
    List<HeatingPhase> getPhases(RequestContext.Device device);

    /**
     * It set the end of the phase to now. In fact the phase
     * is finished.
     * @param requestContext
     */
    void resetPhase(RequestContext requestContext);

    /**
     * Add one hour to the current end of this phase.
     * It does not handle if the end is in past or future.
     * @param type phase which will be postponed
     */
    void postponePhase(RequestContext type);
}
