package miira.moold.heating;

import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumMap;
import java.util.Map;

@Component
public class FlagFiles {

    static final String OFF_FLAG_FILE_NAME = "/home/pi/script/mcp23017/timeStampOffTill";
    static final String ON_FLAG_FILE_NAME = "/home/pi/script/mcp23017/timeStampOnTill";

    static final String OFF_FLAG_RADIATOR_BEDROOM_FILE_NAME = "/home/pi/script/mcp23017/timeStampRadiatorBedRoomOffTill";
    static final String OFF_FLAG_RADIATOR_LIVINGROOM_FILE_NAME = "/home/pi/script/mcp23017/timeStampRadiatorLivingRoomOffTill";
    static final String ON_FLAG_RADIATOR_BEDROOM_FILE_NAME = "/home/pi/script/mcp23017/timeStampRadiatorBedRoomOnTill";
    static final String ON_FLAG_RADIATOR_LIVINGROOM_FILE_NAME = "/home/pi/script/mcp23017/timeStampRadiatorLivingRoomOnTill";

    private final Map<RequestContext.Device, Map<HeatingPhase.Type, Path>> paths = new EnumMap<>(RequestContext.Device.class);

    public FlagFiles() {
        prepareFlagStructure();
        checkFlagFiles();
    }

    void prepareFlagStructure() {
        Map<HeatingPhase.Type, Path> floor = new EnumMap<>(HeatingPhase.Type.class);
        floor.put(HeatingPhase.Type.OFF, Paths.get(OFF_FLAG_FILE_NAME));
        floor.put(HeatingPhase.Type.ON, Paths.get(ON_FLAG_FILE_NAME));
        Map<HeatingPhase.Type, Path> radiatorBedroom = new EnumMap<>(HeatingPhase.Type.class);
        radiatorBedroom.put(HeatingPhase.Type.OFF, Paths.get(OFF_FLAG_RADIATOR_BEDROOM_FILE_NAME));
        radiatorBedroom.put(HeatingPhase.Type.ON, Paths.get(ON_FLAG_RADIATOR_BEDROOM_FILE_NAME));
        Map<HeatingPhase.Type, Path> radiatorLivingroom = new EnumMap<>(HeatingPhase.Type.class);
        radiatorLivingroom.put(HeatingPhase.Type.OFF, Paths.get(OFF_FLAG_RADIATOR_LIVINGROOM_FILE_NAME));
        radiatorLivingroom.put(HeatingPhase.Type.ON, Paths.get(ON_FLAG_RADIATOR_LIVINGROOM_FILE_NAME));
        paths.put(RequestContext.Device.BATHROOM_FLOOR, floor);
        paths.put(RequestContext.Device.RADIATOR_BEDROOM, radiatorBedroom);
        paths.put(RequestContext.Device.RADIATOR_LIVINGROOM, radiatorLivingroom);
    }

    void checkFlagFiles() {
        paths.values().stream()
                .flatMap(a -> a.values().stream())
                .forEach(this::exceptionIfMissing);
    }

    void exceptionIfMissing(Path p) {
        if (Files.notExists(p)) {
            throw new IllegalStateException("Unavailable phase " + p);
        }
    }

    Path getPath(RequestContext requestContext) {
        return paths.get(requestContext.getDevice()).get(requestContext.getPhase());
    }

}
