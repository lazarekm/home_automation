package miira.moold.heating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

/**
 * Signalize time limited cool mode or hot mode by modified time stamp of files on system file.
 */
@Component
public class ForceHeatingFileHelper implements ForceHeatingHelper {

    private final FlagFiles flagFiles;

    public ForceHeatingFileHelper(@Autowired FlagFiles flagFiles) {
        this.flagFiles = flagFiles;
    }

    @Override
    public List<HeatingPhase> getPhases(RequestContext.Device device) {
        HeatingPhase heatingPhaseOff = getHeatingPhase(new RequestContext(device, HeatingPhase.Type.OFF, 0));
        HeatingPhase heatingPhaseOn = getHeatingPhase(new RequestContext(device, HeatingPhase.Type.ON, 0));
        return Arrays.asList(heatingPhaseOff, heatingPhaseOn);
    }

    private HeatingPhase getHeatingPhase(RequestContext context) {
        Path pathOff = flagFiles.getPath(context);
        LocalDateTime offLocalDateTime = getPathModified(pathOff);
        return new HeatingPhase(context.getPhase(), offLocalDateTime);
    }

    @Override
    public void resetPhase(RequestContext requestContext) {
        final LocalDateTime now = LocalDateTime.now();
        Path path = flagFiles.getPath(requestContext);
        setPathModified(path, now);
    }

    @Override
    public void postponePhase(RequestContext requestContext) {
        final Path path = flagFiles.getPath(requestContext);
        LocalDateTime pathDateTime = getPathModified(path);
        LocalDateTime postponedTime = pathDateTime.plusMinutes(requestContext.getAdditionalMinutes());
        setPathModified(path, postponedTime);
    }

    LocalDateTime getPathModified(Path path) {
        FileTime pathModifiedTime = getPathModifiedFileTime(path);
        return LocalDateTime.ofInstant(pathModifiedTime.toInstant(), ZoneId.systemDefault());
    }

    FileTime getPathModifiedFileTime(Path path) {
        try {
            return Files.getLastModifiedTime(path);
        } catch (IOException e) {
            throw new IllegalStateException("Unavailable phase " + path);
        }
    }

    void setPathModified(Path path, LocalDateTime dateTime) {
        Instant instant = dateTime.atZone(ZoneId.systemDefault()).toInstant();
        FileTime nowFileTime = FileTime.from(instant);
        setPathModifiedFileTime(path, nowFileTime);
    }

    void setPathModifiedFileTime(Path path, FileTime nowFileTime) {
        try {
            Files.setLastModifiedTime(path, nowFileTime);
        } catch (IOException e) {
            throw new IllegalStateException("Unavailable phase " + path);
        }
    }
}
