package miira.moold.heating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
public class HeatingPhase {
    public enum Type {
        ON {
            @Override
            public boolean isOn() {
                return true;
            }
        },
        OFF {
            @Override
            public boolean isOff() {
                return true;
            }
        };

        public boolean isOn() {
            return false;
        }

        public boolean isOff() {
            return false;
        }
    }

    Type type;

    LocalDateTime validTill;
}
