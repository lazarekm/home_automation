package miira.moold.heating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * It accept request for prolong heating {@link HeatingPhase.Type#ON} or {@link HeatingPhase.Type#OFF} phase.
 * Handle collisions of both phases, just one of them could be active.
 */
@Component
public class HeatingForcePhaseManager {

    ForceHeatingHelper heatingHelper;

    public HeatingForcePhaseManager(@Autowired ForceHeatingHelper heatingHelper) {
        this.heatingHelper = heatingHelper;
    }

    /**
     * There could be one of these three combination of active phase and requested phase:
     * <ul>
     *     <li>no phase is valid - both of them finished in past. The end time for the requested one is setup as (now+1 hour),</li>
     *     <li>phase X is active - the phase X is requested for prolong. The end time for X phase is prolonged by 1 hour,</li>
     *     <li>phase X is active - the phase Y is requested for prolong. The phase Y is ended, that means that the end time is set
     *     to now. The end time for X phase is set as (now+1 hour).</li>
     * </ul>
     *
     * @param requestContext to be prolonged
     */
    public void prolongPhase(RequestContext requestContext) {
        RequestContext invertPhase = invertPhase(requestContext);
        if (isPhaseActive(invertPhase)) {
            heatingHelper.resetPhase(new RequestContext(requestContext.getDevice(), HeatingPhase.Type.OFF, Long.MAX_VALUE));
            heatingHelper.resetPhase(new RequestContext(requestContext.getDevice(), HeatingPhase.Type.ON, Long.MAX_VALUE));
        } else {
            if (!isPhaseActive(requestContext)) {
                heatingHelper.resetPhase(requestContext);
            }
            heatingHelper.postponePhase(requestContext);
        }
    }

    private boolean isPhaseActive(RequestContext requestContext) {
        final HeatingPhase.Type phase = requestContext.getPhase();
        final RequestContext.Device device = requestContext.getDevice();
        final LocalDateTime now = LocalDateTime.now();
        Optional<HeatingPhase> phaseOptional = heatingHelper.getPhases(device).stream().filter(hp -> hp.getType() == phase).findFirst();
        if (phaseOptional.isPresent()) {
            LocalDateTime phaseTime = phaseOptional.get().getValidTill();
            return phaseTime.isAfter(now);
        }
        throw new IllegalStateException("");
    }

    public List<HeatingPhase> getStatus(RequestContext.Device device) {
        return heatingHelper.getPhases(device);
    }

    private RequestContext invertPhase(RequestContext phaseContext) {
        HeatingPhase.Type invertedPhase = (phaseContext.getPhase().isOn()) ? HeatingPhase.Type.OFF : HeatingPhase.Type.ON;
        return new RequestContext(phaseContext.getDevice(), invertedPhase, phaseContext.getAdditionalMinutes());
    }
}
