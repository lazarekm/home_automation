package miira.moold;

import com.pi4j.gpio.extension.mcp.MCP23017Pin;
import com.pi4j.io.gpio.Pin;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class Mcp23017Util {
    public Optional<Pin> getPinByName(String gpioName) {
        if (gpioName != null) {
            String gpioNameLowerCase = gpioName.toLowerCase();
            for (Pin pin : MCP23017Pin.ALL) {
                String pinName = pin.getName().toLowerCase();
                if (pinName.equals(gpioNameLowerCase) || pinName.equals("gpio " + gpioNameLowerCase)) {
                    return Optional.of(pin);
                }
            }
        }
        return Optional.empty();
    }
}
