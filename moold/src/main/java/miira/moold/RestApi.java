package miira.moold;

import miira.moold.heating.HeatingForcePhaseManager;
import miira.moold.heating.HeatingPhase;
import miira.moold.heating.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class RestApi {

    private final Logger logger = LoggerFactory.getLogger(RestApi.class);

    DeviceController deviceController;

    HeatingForcePhaseManager heatingForcePhaseManager;

    @Autowired
    public RestApi(
            DeviceController deviceController,
            HeatingForcePhaseManager heatingForcePhaseManager
    ) {
        this.deviceController = deviceController;
        this.heatingForcePhaseManager = heatingForcePhaseManager;
    }

    @GetMapping(value = "getDeviceState")
    @CrossOrigin
    public DeviceState getDeviceState(@RequestParam("deviceName") String deviceName) {
        logger.info("Get device state, {}", deviceName);
        // TODO: make lower case formating,
        // TODO: {"deviceName":"LIGHT_SINK","on":false}  ->  {"deviceName":"light_sink","state":"off"}
        return deviceController.getDeviceState(deviceName);
    }

    @GetMapping(value = "setDevice")
    public void setDevice(
            @RequestParam("deviceName") String deviceName,
            @RequestParam("state") String state) {
        // TODO: validate according to enabled devices and requested values in config
        // TODO: check device requested state and required state just here ??
        logger.info("Set device {} to state {}", deviceName, state);
        StateOnOff requestedState = state.trim().equalsIgnoreCase("on") ? StateOnOff.ON : StateOnOff.OFF;
        deviceController.setDevice(deviceName, requestedState);
    }

    /**
     * Set heating mode off or on based on state. Every next call with same state increase
     * time duration by one hour. If there is call with opposite state, then currently valid
     * state is canceled.
     *
     * @param state on or off
     * @return
     */
    @GetMapping(value = "setHeatingTime")
    public List<HeatingPhase> setHeatingTime(
            @RequestParam("state") final String state,
            @RequestParam(value = "hours", required = false) final Optional<Integer> hoursOpt) {
        int minutes = hoursOpt.orElse(1) * 60;
        logger.info("setHeatingTime request state {} for additional {} minute(s)", state, minutes);
        return setStateTime(state, minutes, RequestContext.Device.BATHROOM_FLOOR);
    }

    @GetMapping(value = "setRadiatorStateTime")
    public List<HeatingPhase> setRadiatorStateTime(
            @RequestParam("deviceName") String deviceName,
            @RequestParam("state") final String state,
            @RequestParam(value = "minutes", required = false) final Optional<Integer> minutesOpt) {
        int minutes = minutesOpt.orElse(1);
        logger.info("setRadiatorStateTime for {} request state {} for additional {} minutes(s)", deviceName, state, minutes);
        if (DeviceName.HEATING_BEDROOM.getDeviceName().equals(deviceName)) {
            return setStateTime(state, minutes, RequestContext.Device.RADIATOR_BEDROOM);
        } else if (DeviceName.HEATING_LIVROOM.getDeviceName().equals(deviceName)) {
            return setStateTime(state, minutes, RequestContext.Device.RADIATOR_LIVINGROOM);
        }
        logger.error("Unsupported device name {}", deviceName);
        return Collections.emptyList();
    }

    private List<HeatingPhase> setStateTime(final String state, int minutes, final RequestContext.Device device) {
        // TODO: validate according to enabled devices and requested values in config
        // TODO: check device requested state and required state just here ??
        final String stateNormalized = state.toUpperCase(Locale.ROOT).trim();
        if (minutes > 0) {
            try {
                HeatingPhase.Type phaseName = HeatingPhase.Type.valueOf(stateNormalized);
                final RequestContext requestContext = new RequestContext(
                        device,
                        phaseName,
                        minutes
                );
                heatingForcePhaseManager.prolongPhase(requestContext);
            } catch (IllegalArgumentException e) {
                logger.error(e.getLocalizedMessage());
                turnHeatingOff(device);
            }
        }
        return heatingForcePhaseManager.getStatus(device);
    }

    private void turnHeatingOff(RequestContext.Device device) {
        final RequestContext saveHeatingRequest = new RequestContext(
                device,
                HeatingPhase.Type.OFF,
                Long.MAX_VALUE
        );
        heatingForcePhaseManager.prolongPhase(saveHeatingRequest);
    }

    @GetMapping("startBathroomFan")
    public void startBathroomFan(@RequestParam("durationInSeconds") int durationInSeconds) {
        logger.info("Starting bathroom fan for {} seconds", durationInSeconds);
        deviceController.startBathroomFan(durationInSeconds);
    }
}
