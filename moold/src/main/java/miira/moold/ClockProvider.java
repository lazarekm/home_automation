package miira.moold;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
public class ClockProvider {
    @Bean
    public Clock get() {
        return Clock.systemDefaultZone();
    }
}
