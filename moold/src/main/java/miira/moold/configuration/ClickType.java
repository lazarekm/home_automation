package miira.moold.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClickType implements ConfigurationElement {

    @JsonProperty("name")
    private String name;

    @JsonProperty("durationMin")
    private long durationMinMs;

    @JsonProperty("durationMax")
    private long durationMaxMs;

    public String getName() {
        return name;
    }

    public long getDurationMinMs() {
        return durationMinMs;
    }

    public long getDurationMaxMs() {
        return durationMaxMs;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        visitor.visitClickType(this);
    }
}
