package miira.moold.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeviceDef implements ConfigurationElement {

    @JsonProperty("name")
    private String name;

    @JsonProperty("relayId")
    private String relayId;

    public String getName() {
        return name;
    }

    public String getRelayId() {
        return relayId;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        visitor.visitDevice(this);
    }
}
