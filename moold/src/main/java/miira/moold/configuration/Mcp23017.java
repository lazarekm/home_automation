package miira.moold.configuration;

import com.pi4j.io.i2c.I2CBus;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Getter
@Component
public class Mcp23017 {
    private final int busNumber = I2CBus.BUS_1;
    private final int address = 0x20;
    private final Map<String, String> gpioByDeviceName;

    public Mcp23017() {
        gpioByDeviceName = new HashMap<>();
        gpioByDeviceName.put("fan_speed_bathroom", "A0");
        gpioByDeviceName.put("fan_bathroom", "A1");
        gpioByDeviceName.put("fan_clack_bathroom", "A2");
        gpioByDeviceName.put("heating_livroom", "A3");
        gpioByDeviceName.put("heating_bedroom", "A4");
        gpioByDeviceName.put("heating_bathroom", "B0");
        gpioByDeviceName.put("light_bedroom_left", "B1");
        gpioByDeviceName.put("light_bedroom_right", "B2");
        gpioByDeviceName.put("light_bathroom", "B3");
        gpioByDeviceName.put("light_sink", "B4");
        gpioByDeviceName.put("light_bed", "B5");
        gpioByDeviceName.put("light_corridor", "B6");
    }
}
