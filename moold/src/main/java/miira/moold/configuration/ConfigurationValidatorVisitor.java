package miira.moold.configuration;

import miira.moold.configuration.switchrule.Click;
import miira.moold.configuration.switchrule.ClickRule;
import miira.moold.configuration.switchrule.DeviceState;
import miira.moold.configuration.switchrule.SwitchRule;

import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

public class ConfigurationValidatorVisitor implements ConfigurationVisitor {

    private final Set<String> switchNames = new HashSet<>();

    private final Set<String> deviceNames = new HashSet<>();

    private final Set<String> clickTypeNames = new HashSet<>();

    private final Set<String> switchRuleNames = new HashSet<>();

    private void checkEmpty(String s, String exMsg) {
        if (s == null || s.length() == 0) {
            throw new RuntimeException(exMsg);
        }
    }

    @Override
    public void visitSwitch(SwitchDef switchDef) {
        String name = switchDef.getName();
        checkEmpty(name, "Switch name is empty");
        if (switchNames.contains(name)) {
            throw new RuntimeException("Duplicate switch name: " + name);
        }
        switchNames.add(name);


    }

    @Override
    public void visitDevice(DeviceDef device) {
        String name = device.getName();
        checkEmpty(name, "Device name is empty");
        if (deviceNames.contains(name)) {
            throw new RuntimeException("Duplicate device name: " + name);
        }
        deviceNames.add(name);
        String relayId = device.getRelayId();
        if (relayId == null || relayId.length() == 0) {
            throw new RuntimeException("Relay ID for device \"" + name + "\" is empty");
        }
    }

    @Override
    public void visitClickSpecification(ClickSpecification clickSpecification) {
        long interval = clickSpecification.getDoubleClickInterval();
        if (interval < 0 || interval > 3000) {
            throw new RuntimeException("Double click interval is out of wise range <0, 3000>, actual value: " + interval);
        }
    }

    @Override
    public void visitClickType(ClickType clickType) {
        String name = clickType.getName();
        checkEmpty(name, "Click type name is empty");
        if (clickTypeNames.contains(name)) {
            throw new RuntimeException("Duplicate switch name in clickType, name: " + name);
        }
        clickTypeNames.add(name);

        long durationMinMs = clickType.getDurationMinMs();
        if (durationMinMs < 0 || durationMinMs > 3000) {
            throw new IllegalStateException("Click \"" + name + "\" minimal duration is out of wise range <0, 3000>, actual value: " + durationMinMs);
        }

        long durationMaxMs = clickType.getDurationMaxMs();
        if (durationMaxMs < 0 || durationMaxMs > 3000) {
            throw new IllegalStateException("Click \"" + name + "\" maximal duration is out of wise range <0, 3000>, actual value: " + durationMaxMs);
        }
    }

    @Override
    public void visitSwitchRule(SwitchRule switchRule) {
        String switchRuleName = switchRule.getSwitchName();
        checkEmpty(switchRuleName, "Switch rule name is empty");
        if (switchRuleNames.contains(switchRuleName)) {
            throw new IllegalStateException("Duplicate switch rule name: " + switchRuleName);
        }
        // switchRule name refers switch name
        if (!switchNames.contains(switchRuleName)) {
            throw new NoSuchElementException("Switch rule refers unknown switch name: " + switchRuleName);
        }
        switchRuleNames.add(switchRuleName);
    }

    @Override
    public void visitClick(Click click) {
        String clickName = click.getClickName();
        checkEmpty(clickName, "Click name is empty");
        if (!clickTypeNames.contains(clickName)) {
            throw new NoSuchElementException("Switch rule refers unknown click name: " + clickName);
        }
    }

    @Override
    public void visitConditionAnd(DeviceState condition) {
        commonDeviceChecks(condition, "ConditionAnd");
    }

    @Override
    public void visitConditionOr(DeviceState condition) {
        commonDeviceChecks(condition, "ConditionOr");
    }

    @Override
    public void visitAction(DeviceState action) {
        commonDeviceChecks(action, "Action");
    }

    private void commonDeviceChecks(DeviceState deviceState, String conditionType) {
        String device = deviceState.getDevice();
        checkEmpty(device, conditionType + " device name is empty");
        if (!deviceNames.contains(device)) {
            throw new NoSuchElementException(conditionType + " device refers unknown device name: " + device);
        }

        String state = deviceState.getState();
        if (!state.equals("on") && !state.equals("off")) {
            throw new IllegalStateException("Unsupported device state: " + state);
        }
    }

    @Override
    public void visitClickRule(ClickRule clickRule) {
        DeviceState action = clickRule.getAction();
        if (null == action) {
            throw  new IllegalStateException("Missing switch rule action.");
        }
        List<DeviceState> conditionsAnd = clickRule.getConditionsAnd();
        List<DeviceState> conditionsOr = clickRule.getConditionsOr();
        if ((null == conditionsAnd || conditionsAnd.isEmpty())
                && (null == conditionsOr || conditionsOr.isEmpty())) {
            throw new IllegalStateException("Missing condition for switch rule");
        }
    }

    @Override
    public void visitConfiguration(Configuration configuration) {
        for (String switchName : switchNames) {
            if (!switchRuleNames.contains(switchName)) {
                throw new IllegalStateException("Missing rule for switch: " + switchName);
            }
        }
    }
}
