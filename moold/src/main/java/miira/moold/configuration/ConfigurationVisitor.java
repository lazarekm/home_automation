package miira.moold.configuration;

import miira.moold.configuration.switchrule.Click;
import miira.moold.configuration.switchrule.ClickRule;
import miira.moold.configuration.switchrule.DeviceState;
import miira.moold.configuration.switchrule.SwitchRule;

public interface ConfigurationVisitor {

    void visitSwitch(SwitchDef switchDef);

    void visitDevice(DeviceDef device);

    void visitClickSpecification(ClickSpecification clickSpecification);

    void visitClickType(ClickType clickType);

    void visitSwitchRule(SwitchRule switchRule);

    void visitClick(Click click);

    void visitConditionAnd(DeviceState condition);

    void visitConditionOr(DeviceState condition);

    void visitAction(DeviceState action);

    void visitClickRule(ClickRule clickRule);

    void visitConfiguration(Configuration configuration);
}
