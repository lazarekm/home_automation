package miira.moold.configuration.switchrule;

import com.fasterxml.jackson.annotation.JsonProperty;
import miira.moold.configuration.ConfigurationElement;
import miira.moold.configuration.ConfigurationVisitor;

import java.util.List;

public class SwitchRule implements ConfigurationElement {

    @JsonProperty("switch")
    private String switchName;

    @JsonProperty("clicks")
    private List<Click> clicks;

    public String getSwitchName() {
        return switchName;
    }

    public List<Click> getClicks() {
        return clicks;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        clicks.forEach(c -> c.accept(visitor));
        visitor.visitSwitchRule(this);
    }
}
