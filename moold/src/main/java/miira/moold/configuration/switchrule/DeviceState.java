package miira.moold.configuration.switchrule;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeviceState {

    @JsonProperty("device")
    private String device;

    @JsonProperty("state")
    private String state;

    public String getDevice() {
        return device;
    }

    public String getState() {
        return state;
    }
}
