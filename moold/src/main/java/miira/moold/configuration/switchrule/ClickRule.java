package miira.moold.configuration.switchrule;

import com.fasterxml.jackson.annotation.JsonProperty;
import miira.moold.configuration.ConfigurationElement;
import miira.moold.configuration.ConfigurationVisitor;

import java.util.List;

public class ClickRule implements ConfigurationElement {

    @JsonProperty("conditionsAnd")
    private List<DeviceState> conditionsAnd;

    @JsonProperty("conditionsOr")
    private List<DeviceState> conditionsOr;

    @JsonProperty("action")
    private DeviceState action;

    public List<DeviceState> getConditionsAnd() {
        return conditionsAnd;
    }

    public List<DeviceState> getConditionsOr() {
        return conditionsOr;
    }

    public DeviceState getAction() {
        return action;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        visitor.visitClickRule(this);
        if (null != conditionsAnd) {
            conditionsAnd.forEach(visitor::visitConditionAnd);
        }
        if (null != conditionsOr) {
            conditionsOr.forEach(visitor::visitConditionOr);
        }
        visitor.visitAction(action);
    }
}
