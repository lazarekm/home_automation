package miira.moold.configuration.switchrule;

import com.fasterxml.jackson.annotation.JsonProperty;
import miira.moold.configuration.ConfigurationElement;
import miira.moold.configuration.ConfigurationVisitor;

import java.util.List;

public class Click implements ConfigurationElement {

    @JsonProperty("click")
    private String clickName;

    @JsonProperty("rules")
    private List<ClickRule> rules;

    public String getClickName() {
        return clickName;
    }

    public List<ClickRule> getRules() {
        return rules;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        rules.forEach(r -> r.accept(visitor));
        visitor.visitClick(this);
    }
}
