package miira.moold.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ClickSpecification implements ConfigurationElement {

    @JsonProperty("clickTypes")
    private List<ClickType> clickTypes;

    @JsonProperty("doubleClickInterval")
    private long doubleClickInterval;

    public List<ClickType> getClickTypes() {
        return clickTypes;
    }

    public long getDoubleClickInterval() {
        return doubleClickInterval;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        clickTypes.forEach(ct -> ct.accept(visitor));
        visitor.visitClickSpecification(this);
    }
}
