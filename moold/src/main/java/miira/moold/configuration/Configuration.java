package miira.moold.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import miira.moold.configuration.switchrule.SwitchRule;

import java.util.List;

public class Configuration implements ConfigurationElement {

    @JsonProperty("switches")
    private List<SwitchDef> switches;

    @JsonProperty("devices")
    private List<DeviceDef> devices;

    @JsonProperty("clickSpecification")
    private ClickSpecification clickSpecification;

    @JsonProperty("switchRules")
    private List<SwitchRule> switchRules;

    public List<SwitchDef> getSwitches() {
        return switches;
    }

    public List<DeviceDef> getDevices() {
        return devices;
    }

    public ClickSpecification getClickSpecification() {
        return clickSpecification;
    }

    public List<SwitchRule> getSwitchRules() {
        return switchRules;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        switches.forEach(s -> s.accept(visitor));
        devices.forEach(d -> d.accept(visitor));
        clickSpecification.accept(visitor);
        switchRules.forEach(sr -> sr.accept(visitor));
        visitor.visitConfiguration(this);
    }
}
