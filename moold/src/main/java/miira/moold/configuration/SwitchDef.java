package miira.moold.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SwitchDef implements ConfigurationElement {

    @JsonProperty("name")
    private String name;

    @JsonProperty("enabled")
    private boolean enabled;

    public String getName() {
        return name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void accept(ConfigurationVisitor visitor) {
        visitor.visitSwitch(this);
    }
}
