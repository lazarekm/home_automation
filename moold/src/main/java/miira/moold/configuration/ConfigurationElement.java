package miira.moold.configuration;

public interface ConfigurationElement {
    void accept(ConfigurationVisitor visitor);
}
