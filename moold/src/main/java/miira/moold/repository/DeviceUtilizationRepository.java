package miira.moold.repository;

import miira.moold.model.DeviceUtilization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceUtilizationRepository extends JpaRepository<DeviceUtilization, Long> {
    List<DeviceUtilization> findDeviceUtilizationByDeviceNameAndOffTimeStampIsNull(String deviceName);
}
