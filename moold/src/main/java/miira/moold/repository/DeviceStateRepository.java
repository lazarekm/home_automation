package miira.moold.repository;

import miira.moold.model.DeviceState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceStateRepository extends JpaRepository<DeviceState, String> {
    DeviceState findDeviceStateByDeviceName(String deviceName);
}
