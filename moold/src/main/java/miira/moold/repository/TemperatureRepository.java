package miira.moold.repository;

import miira.moold.model.Temperature;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemperatureRepository extends JpaRepository<Temperature, Long> {
    Temperature findTemperatureBySensorId(String sensorId);
}
