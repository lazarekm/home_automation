package miira.moold;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class SwitchListener {
    private final Logger logger = LoggerFactory.getLogger(SwitchListener.class);


    private static final String L_SINK = "light_sink";
    private static final String L_CORRIDOR = "light_corridor";
    private static final String L_BEDROOM = "light_bed";
    private static final String L_BEDROOM_LEFT = "light_bedroom_left";

    private final RaspberryFactory gpioFactory;
    private final DeviceController deviceController;
    private GpioController gpioController = null;

    @Autowired
    public SwitchListener(RaspberryFactory gpioFactory, DeviceController deviceController) {
        this.gpioFactory = gpioFactory;
        this.deviceController = deviceController;
    }

    @PostConstruct
    void postConstruct() {
        gpioController = gpioFactory.getInstance();
        GpioProvider gpioProvider = gpioFactory.getGpioProvider();
        provisionSwitch(gpioProvider, L_SINK, RaspiPin.GPIO_18);
        provisionSwitch(gpioProvider, L_CORRIDOR, RaspiPin.GPIO_23);
        //provisionSwitch(gpioProvider, L_CORRIDOR, RaspiPin.GPIO_08);
        provisionSwitch(gpioProvider, L_BEDROOM, RaspiPin.GPIO_12);
        provisionSwitch(gpioProvider, L_BEDROOM_LEFT, RaspiPin.GPIO_16);
    }

    private void provisionSwitch(final GpioProvider gpioProvider, final String name, final Pin pin) {
        GpioPinDigitalInput switchPin = gpioController.provisionDigitalInputPin(gpioProvider, pin, name, PinPullResistance.OFF);
        switchPin.setDebounce(50);
        switchPin.addListener((GpioPinListenerDigital) this::notifyRelayController);
    }

    private void notifyRelayController(GpioPinDigitalStateChangeEvent event) {
        logger.info(" --> GPIO PIN STATE CHANGE: {} = {}", event.getPin(), event.getState());
        if (event.getState() == PinState.LOW) {
            // TODO: put it do the configuration
            switch (event.getPin().getPin().getAddress()) {
                case 18:
                    deviceController.switchReleased(L_SINK);
                    break;
                case 23:
                case 8:
                    deviceController.switchReleased(L_CORRIDOR);
                    break;
                case 12:
                    deviceController.switchReleased(L_BEDROOM);
                    break;
                case 16:
                    deviceController.switchReleased(L_BEDROOM_LEFT);
                    break;
                default:
                    logger.warn("pin not supported, name: {}, address: {}, name {}",
                            event.getPin().getName(),
                            event.getPin().getPin().getAddress(),
                            event.getPin().getPin().getName());
            }
        }
    }

    @PreDestroy
    private void preDestroy() {
        if (gpioController != null) {
            gpioController.shutdown();
        }
    }

}
