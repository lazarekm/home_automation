package miira.moold;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
@AllArgsConstructor
public enum DeviceName {

    LIGHT_SINK("B.4", "light_sink"),
    LIGHT_BATHROOM("B.3", "light_bathroom"),
    LIGHT_BED("B.5", "light_bed"),
    LIGHT_CORRIDOR("B.6", "light_corridor"),
    LIGHT_BEDROOM_RIGHT("B.2", "light_bedroom_right"),
    LIGHT_BEDROOM_LEFT("B.1", "light_bedroom_left"),
    HEATING_BATHROOM("B.0", "heating_bathroom"),
    HEATING_BEDROOM("A.4", "heating_bedroom"),
    HEATING_LIVROOM("A.3", "heating_livroom"),
    FAN_CLACK_BATHROOM("A.2", "fan_clack_bathroom"),
    FAN_BATHROOM("A.1", "fan_bathroom"),
    FAN_SPEED_BATHROOM("A.0", "fan_speed_bathroom");

    private final String relayId;
    private final String deviceName;

    public static Optional<DeviceName> getEnumByDeviceName(final String name) {
        return Arrays.stream(DeviceName.values()).filter(e -> e.getDeviceName().equals(name)).findAny();
    }

}
