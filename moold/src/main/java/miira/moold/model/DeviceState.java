package miira.moold.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import miira.moold.StateOnOff;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "devicestate")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceState {
    @Id
    @Column(name = "devicename", length = 50)
    private String deviceName;

    @Column(name="state", length = 3, nullable = false)
    private StateOnOff state;

    @Column(name="lastupdate", nullable = false)
    private LocalDateTime lastUpdate;
}
