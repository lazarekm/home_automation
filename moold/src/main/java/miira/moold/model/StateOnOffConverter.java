package miira.moold.model;

import miira.moold.StateOnOff;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class StateOnOffConverter implements AttributeConverter<StateOnOff, String> {
    @Override
    public String convertToDatabaseColumn(StateOnOff state) {
        return state.name();
    }

    @Override
    public StateOnOff convertToEntityAttribute(String persistedState) {
        return Stream.of(StateOnOff.values())
                .filter(c -> c.name().equals(persistedState))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
