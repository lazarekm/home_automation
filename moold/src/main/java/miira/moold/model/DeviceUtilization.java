package miira.moold.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "deviceutilization")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceUtilization {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "devicename", length = 50, nullable = false)
    private String deviceName;

    @Column(name = "ontimestamp", nullable = false)
    private LocalDateTime onTimeStamp;

    @Column(name = "offtimestamp")
    private LocalDateTime offTimeStamp;
}
