create table sensor (
    id int auto_increment primary key, 
    sensorid varchar(64),
    description varchar(256)
);
insert into sensor values (0, '6c:ec:eb:4b:58:73', 'test temperature sensor on floor bathroom');

create table event (
    id int auto_increment primary key, 
    description varchar(256)
);
insert into event values (0, 'temperature sensor value');
insert into event values (1, 'battery voltage');

create table sensordata (
    id int auto_increment primary key, 
    sensor_id int, 
    event int, 
    datetime datetime,
    value double,
    foreign key (sensor_id) references sensor(id),
    foreign key (event) references event(id)
);

create table relaystate (
    id varchar(5) primary key, 
    ison boolean
);

insert into relaystate values ('A.1', 0);
insert into relaystate values ('A.2', 0);
insert into relaystate values ('A.3', 0);
insert into relaystate values ('A.4', 0);
insert into relaystate values ('A.5', 0);
insert into relaystate values ('A.6', 0);
insert into relaystate values ('B.0', 0);
insert into relaystate values ('B.1', 0);
insert into relaystate values ('B.2', 0);
insert into relaystate values ('B.3', 0);
insert into relaystate values ('B.4', 0);
insert into relaystate values ('B.5', 0);
insert into relaystate values ('B.6', 0);

